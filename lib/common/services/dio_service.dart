import 'package:dio/dio.dart';
import 'package:flutter_application_1/common/utils/constants.dart';

class DioService {
  final dio = Dio(); // With default `Options`.
  final phpDio = Dio();

  DioService() {
    configureDio();
  }

  void configureDio() {
    final options = BaseOptions(
      connectTimeout: const Duration(seconds: 5),
    );
    dio.options = options.copyWith(baseUrl: ApiConstant.baseApiUrl);

    phpDio.options = options.copyWith(baseUrl: ApiConstant.baseApiPhpUrl);
  }
}
