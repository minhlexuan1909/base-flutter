import 'package:flutter_application_1/common/services/navigator_service.dart';
import 'package:flutter_application_1/common/utils/functions.dart';
import 'package:flutter_application_1/notification/domain/entities/notification.dart';
import 'package:flutter_local_notifications/flutter_local_notifications.dart';
import 'package:get_it/get_it.dart';
import 'dart:convert';

@pragma('vm:entry-point')
Future selectNotification(NotificationResponse? payload) async {
  NavigatorService navigatorService = GetIt.instance<NavigatorService>();
  NotificationData? notificationData =
      NotificationData.fromJson(json.decode(payload?.payload ?? ""));

  navigatorService.pushed(notificationData.redirectTo);
}

class NotificationService {
  final FlutterLocalNotificationsPlugin notificationsPlugin =
      FlutterLocalNotificationsPlugin();

  Future<void> initNotification() async {
    AndroidInitializationSettings initializationSettingsAndroid =
        const AndroidInitializationSettings('flutter_logo');

    var initializationSettingsIOS = DarwinInitializationSettings(
        requestAlertPermission: true,
        requestBadgePermission: true,
        requestSoundPermission: true,
        onDidReceiveLocalNotification:
            (int id, String? title, String? body, String? payload) async {});

    var initializationSettings = InitializationSettings(
        android: initializationSettingsAndroid, iOS: initializationSettingsIOS);
    await notificationsPlugin.initialize(initializationSettings,
        onDidReceiveNotificationResponse: selectNotification,
        onDidReceiveBackgroundNotificationResponse: selectNotification);
  }

  generateNotificationDetails(
      {BigPictureStyleInformation? styleInformation,
      AndroidBitmap<Object>? largeIcon}) {
    return NotificationDetails(
        android: AndroidNotificationDetails('channelId', 'channelName',
            importance: Importance.max,
            playSound: true,
            sound: const RawResourceAndroidNotificationSound('test'),
            largeIcon: largeIcon,
            styleInformation: styleInformation),
        iOS: const DarwinNotificationDetails());
  }

  Future showNotification({int id = 0, NotificationData? notification}) async {
    late NotificationDetails notificationDetails =
        generateNotificationDetails();

    ByteArrayAndroidBitmap? imageBitmap, largeIconBitmap;

    if (notification?.image != null) {
      imageBitmap =
          await generateAndroidBitmapFromUrl(notification?.image ?? "");
    }
    if (notification?.largeIcon != null) {
      largeIconBitmap =
          await generateAndroidBitmapFromUrl(notification?.largeIcon ?? "");
    }

    BigPictureStyleInformation? bigPictureStyleInformation =
        imageBitmap != null ? BigPictureStyleInformation(imageBitmap) : null;

    notificationDetails = generateNotificationDetails(
        styleInformation: bigPictureStyleInformation,
        largeIcon: largeIconBitmap);

    return notificationsPlugin.show(id, notification?.title ?? "",
        notification?.body ?? "", notificationDetails,
        payload: json.encode(notification?.toJson()));
  }
}
