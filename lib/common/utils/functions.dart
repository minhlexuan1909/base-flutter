import 'dart:convert';

import 'package:dio/dio.dart';
import 'package:flutter_application_1/common/services/dio_service.dart';
import 'package:flutter_local_notifications/flutter_local_notifications.dart';
import 'package:get_it/get_it.dart';

Future<ByteArrayAndroidBitmap> generateAndroidBitmapFromUrl(
    String imgUrl) async {
  late Dio dio;
  if (GetIt.instance.isRegistered<DioService>()) {
    dio = GetIt.instance<DioService>().dio;
  } else {
    dio = Dio();
  }
  Response response =
      await dio.get(imgUrl, options: Options(responseType: ResponseType.bytes));

  return ByteArrayAndroidBitmap.fromBase64String(base64Encode(response.data));
}
