import 'package:flutter_application_1/common/services/navigator_service.dart';
import 'package:flutter_application_1/notification/data/repositories/data_notification_repository.dart';
import 'package:flutter_application_1/notification/domain/repositories/notification_repository.dart';
import 'package:flutter_application_1/posts/data/repositories/data_post_repository.dart';
import 'package:flutter_application_1/posts/domain/repositories/post_repository.dart';
import 'package:flutter_application_1/common/services/dio_service.dart';
import 'package:get_it/get_it.dart';

// repository
// mobile DB (indexedDB)
// shared preference (local storage) (thong tin khong qua nhay cam va nho)
GetIt getIt = GetIt.instance;
void registRepositoryInjection() {
  getIt.registerLazySingleton<PostRepository>(() => DataPostRepository());
  getIt.registerSingleton<NotificationRepository>(DataNotificationRepository(
      vapidKey: const String.fromEnvironment('VAPID_KEY')));
}

void registServiceInjection() {
  getIt.registerSingleton<DioService>(DioService());
  getIt.registerLazySingleton<NavigatorService>(() => NavigatorService());
}
