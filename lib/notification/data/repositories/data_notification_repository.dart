import 'dart:async';

import 'package:firebase_messaging/firebase_messaging.dart';
import 'package:flutter_application_1/common/services/notify_service.dart';
import 'package:flutter_application_1/notification/domain/entities/topic.dart';
import 'package:flutter_application_1/notification/domain/entities/notification.dart';
import 'package:flutter_application_1/notification/domain/repositories/notification_repository.dart';
import 'package:flutter_application_1/notification/utils/functions.dart';
import 'package:rxdart/rxdart.dart';

@pragma('vm:entry-point')
Future<void> handleBackgroundMessage(RemoteMessage message) async {
  NotificationService().showNotification(
    notification: getNotificationFromRemoteMessage(message),
  );
}

/// {@template notification_repository}
/// A repository for the Notifications domain baked with [FirebaseMessaging].
/// {@endtemplate}
class DataNotificationRepository implements NotificationRepository {
  /// {@macro notification_repository}
  DataNotificationRepository({
    String? vapidKey,
    FirebaseMessaging? firebaseMessaging,
    Stream<RemoteMessage>? onNotificationOpened,
    Stream<RemoteMessage>? onForegroundNotification,
  })  : _vapidKey = vapidKey,
        _firebaseMessaging = firebaseMessaging ?? FirebaseMessaging.instance,
        _onForegroundNotification =
            onForegroundNotification ?? FirebaseMessaging.onMessage,
        _onNotificationOpenedController = BehaviorSubject<NotificationData>() {
    // _initialize(onNotificationOpened ?? FirebaseMessaging.onMessageOpenedApp);
  }

  final String? _vapidKey;
  final FirebaseMessaging _firebaseMessaging;
  final Stream<RemoteMessage> _onForegroundNotification;
  final BehaviorSubject<NotificationData> _onNotificationOpenedController;

  Future<void> initialize({Stream<RemoteMessage>? onNotificationOpened}) async {
    onNotificationOpened ??= FirebaseMessaging.onMessageOpenedApp;
    final response = await _firebaseMessaging.requestPermission();
    final token = await _firebaseMessaging.getToken(vapidKey: _vapidKey);
    print("token $token");

    final status = response.authorizationStatus;
    print("status $status");
    if (status == AuthorizationStatus.authorized) {
      final message = await _firebaseMessaging.getInitialMessage();
      FirebaseMessaging.onBackgroundMessage(handleBackgroundMessage);
      await _sendTokenToServer(token!);
      if (message != null) {
        _onMessageOpened(message);
      }
      onNotificationOpened.listen(_onMessageOpened);
    }
  }

  Future<void> _sendTokenToServer(String token) {
    return Future.sync(() => null);
  }

  void _onMessageOpened(RemoteMessage message) {
    _onNotificationOpenedController.sink
        .add(getNotificationFromRemoteMessage(message));
  }

  /// Returns a [Stream] that emits when a user presses a [NotificationData]
  /// message displayed via FCM.
  ///
  /// If your app is opened via a notification whilst the app is terminated,
  /// see [FirebaseMessaging.getInitialMessage].
  @override
  Stream<NotificationData> get onNotificationOpened {
    return _onNotificationOpenedController.stream;
  }

  /// Returns a [Stream] that emits when an incoming [NotificationData] is
  /// received whilst the Flutter instance is in the foreground.
  @override
  Stream<NotificationData> get onForegroundNotification {
    return _onForegroundNotification.mapNotNull((message) {
      return getNotificationFromRemoteMessage(message);
    });
  }

  /// Subscribe to topic in background.
  @override
  Future<void> subscribeToTopic(Topic topic) async {
    await _firebaseMessaging.subscribeToTopic(topic.name);
  }

  /// Unsubscribe from topic in background.
  @override
  Future<void> unsubscribeFromTopic(Topic topic) async {
    await _firebaseMessaging.unsubscribeFromTopic(topic.name);
  }
}
