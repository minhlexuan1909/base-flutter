import 'package:freezed_annotation/freezed_annotation.dart';
import 'package:collection/collection.dart';
part 'notification.freezed.dart';
part 'notification.g.dart';

enum NotificationScope { global, post }

@Freezed()
class NotificationData with _$NotificationData {
  const factory NotificationData({
    required String title,
    required String body,
    @NotificationScopeJsonConverter() required NotificationScope scope,
    @Default("") String image,
    @Default("") String largeIcon,
    @Default("") String redirectTo,
  }) = _NotificationData;

  factory NotificationData.fromJson(Map<String, dynamic> json) =>
      _$NotificationDataFromJson(json);
}

class NotificationScopeJsonConverter
    implements JsonConverter<NotificationScope, String> {
  const NotificationScopeJsonConverter();

  @override
  NotificationScope fromJson(String notificationScopeString) {
    return NotificationScope.values.firstWhere((element) =>
        element.toString() == "NotificationScope.$notificationScopeString");
  }

  @override
  String toJson(NotificationScope notificationScope) => notificationScope
      .toString()
      .replaceAll("NotificationScope.", "")
      .toLowerCase();
}
