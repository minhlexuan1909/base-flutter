// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'notification.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

_$NotificationDataImpl _$$NotificationDataImplFromJson(
        Map<String, dynamic> json) =>
    _$NotificationDataImpl(
      title: json['title'] as String,
      body: json['body'] as String,
      scope: const NotificationScopeJsonConverter()
          .fromJson(json['scope'] as String),
      image: json['image'] as String? ?? "",
      largeIcon: json['largeIcon'] as String? ?? "",
      redirectTo: json['redirectTo'] as String? ?? "",
    );

Map<String, dynamic> _$$NotificationDataImplToJson(
        _$NotificationDataImpl instance) =>
    <String, dynamic>{
      'title': instance.title,
      'body': instance.body,
      'scope': const NotificationScopeJsonConverter().toJson(instance.scope),
      'image': instance.image,
      'largeIcon': instance.largeIcon,
      'redirectTo': instance.redirectTo,
    };
