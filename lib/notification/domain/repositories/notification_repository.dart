import 'package:firebase_messaging/firebase_messaging.dart';
import 'package:flutter_application_1/notification/domain/entities/notification.dart';
import 'package:flutter_application_1/notification/domain/entities/topic.dart';

abstract class NotificationRepository {
  Future<void> initialize({Stream<RemoteMessage>? onNotificationOpened});
  Stream<NotificationData> get onNotificationOpened;
  Stream<NotificationData> get onForegroundNotification;
  Future<void> subscribeToTopic(Topic topic);
  Future<void> unsubscribeFromTopic(Topic topic);
}
