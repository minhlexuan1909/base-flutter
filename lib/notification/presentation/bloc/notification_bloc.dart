import 'package:equatable/equatable.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import "package:flutter_application_1/notification/domain/entities/notification.dart";

part 'notification_event.dart';
part 'notification_state.dart';

class NotificationBloc extends Bloc<NotificationEvent, NotificationState> {
  NotificationBloc() : super(const NotificationState.initial()) {
    on<_NotificationOpened>(_onNotificationOpened);
    on<_NotificationInForegroundReceived>(_onNotificationInForegroundReceived);
  }

  void _onNotificationOpened(
    _NotificationOpened event,
    Emitter<NotificationState> emit,
  ) {
    emit(
      state.copyWith(
        notification: event.notification,
        appState: AppState.background,
      ),  
    );
  }

  void _onNotificationInForegroundReceived(
    _NotificationInForegroundReceived event,
    Emitter<NotificationState> emit,
  ) {
    emit(
      state.copyWith(
        notification: event.notification,
        appState: AppState.foreground,
      ),
    );
  }
}
