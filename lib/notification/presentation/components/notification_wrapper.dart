import 'package:flutter/material.dart';
import 'package:flutter_application_1/notification/presentation/bloc/notification_bloc.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

class NotificationWrapper extends StatelessWidget {
  const NotificationWrapper({super.key});
  @override
  Widget build(BuildContext context) {
    return BlocBuilder<NotificationBloc, NotificationState>(
      builder: (context, state) {
        switch (state.notification?.title) {
          case "a":
            return const Text("a");
          case "b":
            return const Text("b");
          default:
            return const Text("default");
        }
      },
    );
  }
}
