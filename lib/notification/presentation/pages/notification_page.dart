import 'package:flutter/material.dart';
import 'package:flutter_application_1/notification/presentation/bloc/notification_bloc.dart';
import 'package:flutter_application_1/notification/presentation/components/notification_wrapper.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

class NotificationPage extends StatelessWidget {
  const NotificationPage({super.key});
  @override
  Widget build(BuildContext context) {
    return Scaffold(
        key: GlobalKey(),
        appBar: AppBar(
          title: const Text('Test Page'),
        ),
        body: BlocProvider(
            create: (_) => NotificationBloc(),
            child: const NotificationWrapper()));
  }
}
