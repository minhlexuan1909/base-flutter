import 'package:firebase_messaging/firebase_messaging.dart';
import 'package:flutter_application_1/notification/domain/entities/notification.dart';
import 'package:collection/collection.dart';

NotificationData getNotificationFromRemoteMessage(RemoteMessage message) {
  return NotificationData(
      title: message.data["title"] ?? '',
      body: message.data["title"] ?? '',
      scope: NotificationScope.values.firstWhereOrNull((element) =>
              element.toString() ==
              "NotificationScope.${message.data["scope"]}") ??
          NotificationScope.global,
      image: message.data["image"] ?? "",
      largeIcon: message.data["largeIcon"] ?? "",
      redirectTo: message.data["redirectTo"] ?? "");
}
