import 'package:flutter_application_1/notification/domain/entities/notification.dart';
import 'package:flutter_application_1/notification/domain/repositories/notification_repository.dart';
import 'package:get_it/get_it.dart';
import 'package:rxdart/rxdart.dart';

// Data từ DataNotificationRepository sẽ được cung cấp vào stream onForegroundNotification => NotificationCenter phân loại data này và cung cấp data vào các stream của nó => View lắng nghe stream của NotificationCenter và xử lý
class NotificationCenter {
  static final NotificationCenter _instance = NotificationCenter();

  final BehaviorSubject<NotificationData?> globalNotificationStream =
      BehaviorSubject.seeded(null);
  final BehaviorSubject<NotificationData?> postNotificationStream =
      BehaviorSubject.seeded(null);

  NotificationCenter() {
    GetIt.instance<NotificationRepository>().initialize().then((value) {
      GetIt.instance<NotificationRepository>()
          .onForegroundNotification
          .listen((notification) {
        switch (notification.scope) {
          case NotificationScope.global:
            globalNotificationStream.sink.add(notification);
            break;
          case NotificationScope.post:
            postNotificationStream.sink.add(notification);
            break;
        }
      });
    });
  }

  static NotificationCenter get instance => _instance;
}
