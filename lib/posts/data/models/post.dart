import 'package:flutter_application_1/common/utils/data_mapper.dart';
import 'package:flutter_application_1/posts/domain/entities/post.dart';

class PostResponseModel extends DataMapper<PostEntity> {
  PostResponseModel({
    required this.userId,
    required this.id,
    required this.title,
    required this.body,
  });

  int userId;
  int id;
  String title;
  String body;

  Map<String, dynamic> toJson() => {
        'title': title,
        'id': id,
      };

  @override
  PostEntity mapToEntity() {
    return PostEntity(
      title: title,
      id: id,
    );
  }

  PostResponseModel.fromJson(Map<String, dynamic> map)
      : userId = map['userId'],
        id = map['id'],
        title = map['title'],
        body = map['body'];
}
