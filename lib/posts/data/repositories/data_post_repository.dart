import 'package:flutter_application_1/posts/data/models/post.dart';
import 'package:flutter_application_1/posts/data/utils/constants.dart';
import 'package:flutter_application_1/posts/domain/entities/post.dart';
import 'package:flutter_application_1/posts/domain/repositories/post_repository.dart';
import 'package:flutter_application_1/common/services/dio_service.dart';
import 'package:get_it/get_it.dart';

class DataPostRepository implements PostRepository {
  final dio = GetIt.instance<DioService>().dio;

  static final DataPostRepository _instance = DataPostRepository._internal();
  DataPostRepository._internal();
  factory DataPostRepository() => _instance;

  @override
  Future<List<PostEntity>> getPosts() async {
    try {
      List<PostEntity> posts = List.empty();
      final response = await dio.get<List<dynamic>>(PostApiPath.posts);

      if (response.data != null && response.data!.isNotEmpty) {
        posts = List.from(response.data!.map((map) {
          PostResponseModel postResponseModel = PostResponseModel.fromJson(map);
          return postResponseModel.mapToEntity();
        }));
      }
      return posts;
    } catch (e) {
      return List.empty();
    }
  }
}
