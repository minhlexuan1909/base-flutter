class PostEntity {
  PostEntity({
    required this.title,
    required this.id,
  });

  String title;
  int id;
}
