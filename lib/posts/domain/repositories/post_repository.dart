import 'package:flutter_application_1/posts/domain/entities/post.dart';

abstract class PostRepository {
  Future<List<PostEntity>> getPosts();
}
