import 'dart:async';

import 'package:flutter_application_1/common/services/notify_service.dart';
import 'package:flutter_application_1/notification/domain/entities/notification.dart';
import 'package:flutter_application_1/notification/utils/notification_center.dart';
import 'package:flutter_application_1/posts/domain/repositories/post_repository.dart';
import 'package:flutter_application_1/posts/presentation/bloc/post_event.dart';
import 'package:flutter_application_1/posts/presentation/bloc/post_state.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:get_it/get_it.dart';

class PostBloc extends Bloc<PostEvent, PostState> {
  final PostRepository _postRepository = GetIt.instance<PostRepository>();
  late StreamSubscription<NotificationData?> _streamSubscription;

  PostBloc() : super(const PostState()) {
    _streamSubscription = NotificationCenter.instance.postNotificationStream
        .listen((notification) {
      if ((notification?.title != null && notification?.title != "") ||
          (notification?.body != null && notification?.body != "")) {
        NotificationService().showNotification(notification: notification);
      }
    });
    on<PostFetched>(_onPostFetched);
  }

  Future<void> _onPostFetched(
      PostFetched event, Emitter<PostState> emit) async {
    try {
      emit(state.copyWith(
        status: PostStatus.initial,
      ));
      final posts = await _postRepository.getPosts();
      emit(state.copyWith(
        status: PostStatus.success,
        posts: posts,
        hasReachedMax: false,
      ));
    } catch (_) {
      emit(state.copyWith(status: PostStatus.failure));
    }
  }

  @override
  Future<void> close() {
    _streamSubscription.cancel();
    return super.close();
  }
}
