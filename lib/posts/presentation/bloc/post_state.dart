import 'package:flutter_application_1/posts/domain/entities/post.dart';
import 'package:freezed_annotation/freezed_annotation.dart';
part 'post_state.freezed.dart';

enum PostStatus { initial, success, failure }

@Freezed()
class PostState with _$PostState {
  const factory PostState({
    @Default(PostStatus.initial) PostStatus status,
    @Default([]) List<PostEntity> posts,
    @Default(false) bool hasReachedMax,
  }) = _PostState;
}
