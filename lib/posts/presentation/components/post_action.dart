import 'package:flutter/material.dart';
import 'package:flutter_application_1/posts/presentation/bloc/post_bloc.dart';
import 'package:flutter_application_1/posts/presentation/bloc/post_event.dart';
import 'package:flutter_application_1/posts/presentation/bloc/post_state.dart';
import 'package:flutter_application_1/posts/presentation/components/post_list.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

class PostAction extends StatelessWidget {
  const PostAction({super.key});
  @override
  Widget build(BuildContext context) {
    return BlocBuilder<PostBloc, PostState>(
      builder: (context, state) {
        return Expanded(
            child: Column(
          children: [
            ElevatedButton(
              onPressed: () {
                context.read<PostBloc>().add(PostFetched());
              },
              child: const Text('Get Name'),
            ),
            const PostsList()
          ],
        ));
      },
    );
  }
}
