import 'package:flutter/material.dart';
import 'package:flutter_application_1/posts/presentation/bloc/post_bloc.dart';
import 'package:flutter_application_1/posts/presentation/bloc/post_state.dart';
import 'package:flutter_application_1/posts/presentation/components/post_list_item.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

class PostsList extends StatefulWidget {
  const PostsList({super.key});

  @override
  State<PostsList> createState() => _PostsListState();
}

class _PostsListState extends State<PostsList> {
  final _scrollController = ScrollController();

  @override
  Widget build(BuildContext context) {
    return BlocBuilder<PostBloc, PostState>(
      builder: (context, state) {
        switch (state.status) {
          case PostStatus.failure:
            return const Center(child: Text('failed to fetch posts'));
          case PostStatus.success:
            if (state.posts.isEmpty) {
              return const Center(child: Text('no posts'));
            }
            return Expanded(
              child: ListView.builder(
                  scrollDirection: Axis.vertical,
                  // shrinkWrap: true,
                  itemBuilder: (BuildContext context, int index) {
                    return PostListItem(post: state.posts[index]);
                  },
                  itemCount: state.posts.length,
                  controller: _scrollController),
            );

          case PostStatus.initial:
            return const Center(child: CircularProgressIndicator());
        }
      },
    );
  }

  @override
  void dispose() {
    _scrollController.dispose();
    super.dispose();
  }
}
