import 'package:flutter/material.dart';
import 'package:flutter_application_1/posts/domain/entities/post.dart';

class PostListItem extends StatelessWidget {
  const PostListItem({required this.post, super.key});

  final PostEntity post;

  @override
  Widget build(BuildContext context) {
    final textTheme = Theme.of(context).textTheme;

    return Material(
      child: ListTile(
        leading: Text(
          "${post.id}",
          style: textTheme.bodySmall,
          maxLines: 1,
          overflow: TextOverflow.ellipsis,
        ),
        title: Text(post.title),
        isThreeLine: true,
        subtitle: Text(post.title),
        dense: true,
      ),
    );
  }
}
