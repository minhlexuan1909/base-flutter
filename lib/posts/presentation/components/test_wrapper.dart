import 'package:flutter/material.dart';
import 'package:flutter_application_1/posts/presentation/components/post_action.dart';

class TestWrapper extends StatelessWidget {
  const TestWrapper({super.key});
  @override
  Widget build(BuildContext context) {
    return const Scaffold(
      body: Column(
        children: [PostAction()],
      ),
    );
  }
}
