import 'package:flutter/material.dart';
import 'package:flutter_application_1/posts/presentation/bloc/post_bloc.dart';
import 'package:flutter_application_1/posts/presentation/bloc/post_event.dart';
import 'package:flutter_application_1/posts/presentation/components/test_wrapper.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

class TestPage extends StatefulWidget {
  const TestPage({super.key});

  @override
  State<TestPage> createState() => _TestPageState();
}

class _TestPageState extends State<TestPage> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
        key: GlobalKey(),
        appBar: AppBar(
          title: const Text('Test Page'),
        ),
        body: MultiBlocProvider(
            // ..add(PostFetched()) to call PostFetched() in initState()
            providers: [
              BlocProvider(create: (_) => PostBloc()..add(PostFetched())),
            ],
            child: const TestWrapper()));
  }
}
