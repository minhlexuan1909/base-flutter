import 'package:flutter/material.dart';
import 'package:flutter_application_1/notification/presentation/pages/notification_page.dart';
import 'package:flutter_application_1/pages.dart';

class CustomRouter {
  final RouteObserver<PageRoute> routeObserver;

  CustomRouter() : routeObserver = RouteObserver<PageRoute>();

  Route<dynamic> getRoute(RouteSettings settings) {
    switch (settings.name) {
      case Pages.test:
        return _buildRoute(settings, const TestPage());
      case Pages.notification:
        return _buildRoute(settings, const NotificationPage());
      default:
        return MaterialPageRoute(
          builder: (_) => Scaffold(
            body: Container(),
          ),
        );
    }
  }

  MaterialPageRoute _buildRoute(RouteSettings settings, Widget builder) {
    return MaterialPageRoute(
      settings: settings,
      builder: (ctx) => builder,
    );
  }
}
